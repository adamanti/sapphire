import torch
import torch.nn as nn

from brilliant import *

all_optimizers = {
    "adam": torch.optim.Adam,
    "adagrad": torch.optim.Adagrad
}

all_criteria = {
    "BCELoss": nn.BCELoss,
    "CrossEntropyLoss": nn.CrossEntropyLoss
}


def _build_architecture(bp_architecture):
    model = architecture_mapper[bp_architecture['architecture']](pretrained=bp_architecture['pretrained'])

    # set optimizers from the outside as it requires the existing torch lightning class
    optim_type = bp_architecture['optimizer']['type']
    optim_kwargs = bp_architecture['optimizer']['kwargs']
    model.optim = all_optimizers[optim_type](model.parameters(), **optim_kwargs)
    return model


def _build_model(bp_architectures, borders):
    input_format, output_format = borders['input'], borders['output']

    # construct architectures
    bp_architectures = [_build_architecture(arch) for arch in bp_architectures]

    # construct glue
    connectors = []
    for bp_architecture in bp_architectures:
        connectors.append(Connector(input_format, bp_architecture.input_format))
        input_format = bp_architecture(torch.rand(connectors[-1](torch.rand(input_format)).shape)).shape
    connectors.append(Connector(input_format, output_format))

    # glue architectures together
    model = bp_architectures + connectors
    model[::2] = connectors
    model[1::2] = bp_architectures
    return nn.Sequential(*model)


def _build_criterion(bp_criterion):
    return all_criteria[bp_criterion]()


def _build_metrics(bp_metrics):
    return [metrics_mapper[metric]() for metric in bp_metrics]


def build_blueprint(blueprint):
    bp_model = _build_model(blueprint['architectures'], blueprint['borders'])
    bp_criterion = _build_criterion(blueprint['criterion'])
    bp_metrics = _build_metrics(blueprint['metrics'])
    return bp_model, bp_criterion, bp_metrics
