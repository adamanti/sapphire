from abc import ABC, abstractmethod


class BrilliantMetric(ABC):
    @abstractmethod
    def calculate(self, prediction, ground_truth):
        """calculates and updates metric value (self.value)"""
        raise NotImplementedError
