import torch

from .brilliant_metric import BrilliantMetric


class Accuracy(BrilliantMetric):
    def __init__(self):
        super().__init__()
        self.name = 'accuracy'

    def calculate(self, prediction, ground_truth):
        _, prediction = torch.max(prediction, 1)
        return (prediction == ground_truth).sum() / len(ground_truth)

