from .accuracy import Accuracy


__all__ = ('metrics_mapper', 'Accuracy')


metrics_mapper = {
    "accuracy": Accuracy
}


