import torch
import torch.nn as nn

from .brilliant_architecture import BrilliantArchitecture


class GroundZero(BrilliantArchitecture):
    def __init__(self, pretrained):
        super().__init__()
        self.pretrained = pretrained
        assert not self.pretrained, f"{self} doesn't support pretrained mode yet"

        # noinspection PyTypeChecker
        self.model = nn.Sequential(
            nn.Conv2d(in_channels=5, out_channels=10, kernel_size=2, padding=1),
            nn.LeakyReLU(),
            nn.Conv2d(in_channels=10, out_channels=20, kernel_size=2, padding=1),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            nn.BatchNorm2d(20),

            nn.Conv2d(in_channels=20, out_channels=30, kernel_size=2, padding=1),
            nn.LeakyReLU(),
            nn.Conv2d(in_channels=30, out_channels=40, kernel_size=2, padding=1),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            nn.BatchNorm2d(40),

            nn.Conv2d(in_channels=40, out_channels=50, kernel_size=2, padding=1),
            nn.LeakyReLU(),
            nn.Conv2d(in_channels=50, out_channels=70, kernel_size=2, padding=1),
            nn.LeakyReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2),

            nn.BatchNorm2d(70),
        )

        # default optimizer (might be overwritten by builder)
        self.optim = torch.optim.Adam(self.parameters(), lr=1e-3)

        # required by the Connector (>0=change, <0=doesn't matter)
        self.input_format = (5, -1, -1)
