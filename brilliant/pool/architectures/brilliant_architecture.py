import torch
import pytorch_lightning as pl


class BrilliantArchitecture(pl.LightningModule):
    def forward(self, x):
        if self.pretrained:
            torch.no_grad()

        return self.model(x)

    def configure_optimizers(self):
        return self.optim
