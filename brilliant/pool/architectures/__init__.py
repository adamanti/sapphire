from .ground_zero import GroundZero
from .resnet101 import ResNet101


__all__ = ('architecture_mapper', 'GroundZero', 'ResNet101')


architecture_mapper = {
    "ground_zero": GroundZero,
    "resnet101": ResNet101
}


