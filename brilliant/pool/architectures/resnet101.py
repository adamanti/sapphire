import torch
import torch.nn as nn
from torchvision import models

from .brilliant_architecture import BrilliantArchitecture


class ResNet101(BrilliantArchitecture):
    def __init__(self, pretrained):
        super().__init__()
        self.pretrained = pretrained

        resnet101_backbone = list(models.resnet101(pretrained=pretrained).children())[:-1]
        self.model = nn.Sequential(*resnet101_backbone)

        # default optimizer (might be overwritten by builder)
        self.optim = torch.optim.Adam(self.parameters(), lr=1e-3)

        # required by the Connector (>0=change, <0=doesn't matter)
        self.input_format = (3, -1, -1)
