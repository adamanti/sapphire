import operator
import pytorch_lightning as pl
from functools import reduce

import torch


class Connector(pl.LightningModule):
    # theoretically, input_format is not needed now
    def __init__(self, input_format, output_format):
        super().__init__()
        self.input_format, self.output_format = input_format, output_format

    # the input format is the actual shape that the connector component expects.
    # the output format rather defines a pattern that the output has to comply with
    def forward(self, x):
        # can't we do just "input_format = x.shape" and omit the input_format information in the __init__?
        n_datapoints = reduce(operator.mul, self.input_format, 1)
        output_shape = [int(x.shape[0])]
        counter = 0
        for idx, num in enumerate(self.output_format):
            if num < 0:
                counter += 1

            output_shape.append(num)

        n_datapoints = int(n_datapoints / reduce(operator.mul, list(filter(lambda x: x > 0, output_shape)), 1))
        if counter:
            largest_factors = _find_largest_factors([n_datapoints], counter)
            largest_factors.reverse()
            final_shape = []
            for element in output_shape:
                if element < 0:
                    element = largest_factors.pop()
                final_shape.append(element)
            x = x.flatten()[:reduce(operator.mul, final_shape, 1)].reshape(final_shape)
        else:
            x = x.flatten()[:reduce(operator.mul, output_shape, 1)].reshape(output_shape)
        return x


# these algorithms are relatively inefficient and not practicable for large input data
def _find_largest_factor_in(some_list: [int]):
    largest_factor = 1
    for element in some_list:
        for i in range(2, int(element / 2)):
            if element / i < largest_factor:
                break
            if element % i == 0:
                largest_factor = max(largest_factor, int(element / i))
                break
    return largest_factor


def _find_largest_factors(ret: [int], k: int):
    if len(ret) == k:
        return ret
    else:
        new_factor = _find_largest_factor_in(ret)
        ret.append(new_factor)
        idx = next(idx for idx, x in enumerate(ret) if x % new_factor == 0)
        ret[idx] = int(ret[idx] / new_factor)
        return _find_largest_factors(ret, k)
