from types import SimpleNamespace

from torch.utils.data import DataLoader, random_split
from torchvision import transforms
from torchvision.datasets import CIFAR10
import pytorch_lightning as pl


all_transformations = {
    'toTensor': transforms.ToTensor,
    'normalize': transforms.Normalize
}


class SapphireDataModuleMock(pl.LightningDataModule):
    """
    This class downloads the specified data set split-wise, applies transformations to the data, and finally creates
    data loaders to be served to the Pytorch Lightning Trainer.
    """
    def __init__(self, data_url: str, blueprint: SimpleNamespace) -> None:
        super().__init__()
        self.blueprint = blueprint
        self.data_url = data_url
        self.data_path = "data/" + "mock_directory_cifar_10"
        self.train, self.val, self.test = None, None, None
        # default batch_size (will be overwritten by lightning trainer anyway)
        self.batch_size = 10

    def prepare_data(self):
        CIFAR10(root=self.data_path, download=True)

    def setup(self):
        transformations = []
        try:
            transformations = list(map(lambda tf: all_transformations[tf], self.blueprint['transformations']))
            transformations = transforms.Compose([transforms.ToTensor()])
        except KeyError:
            pass

        target_transformations = []
        try:
            target_transformations = list(map(lambda tf: all_transformations[tf], self.blueprint['target_transformations']))
            target_transformations = transforms.Compose(*target_transformations)
        except KeyError:
            pass

        self.train = CIFAR10(root=self.data_path, train=True, transform=transformations)
        self.train, self.val = random_split(self.train, [int(len(self.train)*0.8), len(self.train)-int(len(self.train)*0.8)])
        self.test = CIFAR10(root=self.data_path, train=False, transform=transformations)

    def train_dataloader(self):
        return DataLoader(self.train, batch_size=self.batch_size, shuffle=True, num_workers=8)

    def val_dataloader(self):
        return DataLoader(self.val, batch_size=self.batch_size, shuffle=False, num_workers=8)

    def test_dataloader(self):
        return DataLoader(self.test, batch_size=self.batch_size, shuffle=False, num_workers=8)
