import hashlib
import json
import random
from datetime import datetime

from analyzer import get_blueprints, get_training_option
from generate import generate
from train import train


if __name__ == "__main__":
    timestamp = datetime.now().strftime("%m-%d-%Y-%H-%M-%S")

    fp = open('mocks/case_mock.json')
    case = json.load(fp)
    case['signature'] = hashlib.md5(str(case).encode("UTF-8")).hexdigest()[:5] + timestamp
    case['timestamp'] = timestamp
    blueprints = get_blueprints(case)

    # mock user decision process. The user can select blueprints to proceed with (code generation, training)
    blueprints_to_generate = random.sample(blueprints, 0)
    blueprints_to_train = random.sample(blueprints, 1)

    for blueprint in blueprints_to_train:
        # prepare environment where train service will be deployed to
        remote_worker = get_training_option(blueprint)
        # later, train job will be deployed on remote_worker
        # it might be reasonable that a user desires to choose different hardware configurations for different
        # blueprints but he/she also has to consider that the dataset has eventually to be downloaded multiple times in
        # this case which would produce an additional overhead (you need the data "close" to in-memory on each machine!)
        train(project_name=case['project'], data_url=case['data_url'], blueprint=blueprint)

    for blueprint in blueprints_to_generate:
        generate(project_name=case['project'], blueprint=blueprint)
