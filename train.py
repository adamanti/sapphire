# python modules
from pathlib import Path

# pytorch modules
import torch

import pytorch_lightning as pl
from pytorch_lightning import loggers as pl_loggers

# project modules
from datamodule import SapphireDataModule
from datamodule_mock import SapphireDataModuleMock
from model import SapphireModel


def train(project_name, data_url, blueprint):
    # prepare tensorboard logger (atm this would be stored only inside the container!)
    tb_logger = pl_loggers.TensorBoardLogger('logs/', name=project_name,
                                             version=blueprint['timestamp'] + blueprint['signature'],
                                             log_graph=True, default_hp_metric=False)
    tb_logger.experiment.add_text(project_name, str(blueprint))

    # prepare pytorch lightning data module
    datamodule = SapphireDataModuleMock(data_url=data_url, blueprint=blueprint['dataloader'])
    datamodule.prepare_data()
    datamodule.setup()

    # prepare pytorch lightning model
    model = SapphireModel(blueprint=blueprint['model'])

    # define pytorch lightning trainer
    trainer = pl.Trainer(logger=tb_logger, max_epochs=blueprint['predictions']['n_epochs'], auto_scale_batch_size=True)

    # train
    trainer.fit(model, datamodule)

    # test
    trainer.test(model, test_dataloaders=datamodule.test_dataloader())

    # save (atm this would be stored only inside the container!)
    Path('output/' + project_name + '/').mkdir(parents=True, exist_ok=True)
    torch.save(model.state_dict(), 'output/' + project_name + '/' + blueprint['timestamp'] + "-" +
               blueprint['signature'])
