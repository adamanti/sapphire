import os
import pickle
from typing import Optional, Callable
from torch.utils.data import Dataset


class SapphireSet(Dataset):
    def __init__(self, url: str, root: str, mode: Optional[str] = None, transform: Optional[Callable] = None,
                 target_transform: Optional[Callable] = None):
        self.url = url
        self.root = root
        self.mode = mode
        self.transform = transform
        self.target_transform = target_transform

        if not self._check_integrity():
            self.download()

        if mode is not None:
            self.data = []
            self.targets = []

            for file_name in self.meta[mode].filenames:
                file_path = os.path.join(self.root, file_name)
                with open(file_path, 'rb') as fp:
                    entry = pickle.load(fp, encoding='latin1')
                    self.data.append(entry['data'])
                    self.targets.append(entry['labels'])

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        data, target = self.data[idx], self.targets[idx]
        if self.transform is not None:
            data = self.transform(data)

        if self.target_transform is not None:
            target = self.target_transform(target)

        return data, target

    def _check_integrity(self):
        # check files in root... for now, mock that the dataset is always missing, incomplete, or corrupted
        return False

    def download(self):
        assert self._check_url(self.url)
        self._download_meta(self.url)
        self._download_train(self.url)
        self._download_test(self.url)

    def _check_url(self, url):
        # validate data url with regex
        pass

    def _download_meta(self, url):
        pass

    def _download_train(self, url):
        pass

    def _download_test(self, url):
        pass
