from urllib.parse import urlparse


def extract_domain(some_url):
    return urlparse(some_url).netloc


# OBSOLETE FUNCTIONS THAT I DON'T WANT TO DELETE YET #
"""
# file: utils.py
def unpickle(file):
    with open(file, 'rb') as fo:
        return pickle.load(fo, encoding='bytes')

# file: model.py, class: SapphireModel
def visualize_predictions(self, images, preds, labels, mode='train'):
    dataset_meta = unpickle('datasets/cifar100/cifar-100-python/meta')
    class_names = list(map(lambda x: x.decode('utf-8'), dataset_meta.get(b'fine_label_names')))

    # determine size of the grid based on given batch size
    num_rows = math.floor(math.sqrt(len(images)))

    fig = plt.figure(figsize=(16, 16))
    for i in range(len(images)):
        plt.subplot(num_rows, len(images) // num_rows + 1, i + 1)
        plt.imshow(np.uint8(images[i].permute(1, 2, 0) * 255))
        plt.title('Pred: ' + class_names[torch.argmax(preds, dim=-1)[i]] +
                  '\nLabel: ' + class_names[labels[i]])
        plt.axis('off')

    self.logger.experiment.add_figure('predictions [' + mode + ']', fig, global_step=self.global_step)
"""
