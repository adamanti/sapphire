import hashlib
from types import SimpleNamespace

from torch.utils.data import DataLoader
from torchvision import transforms
import pytorch_lightning as pl

from dataset import SapphireSet


all_transformations = {
    'toTensor': transforms.ToTensor,
    'normalize': transforms.Normalize
}


class SapphireDataModule(pl.LightningDataModule):
    """
    This class downloads the specified data set split-wise, applies transformations to the data, and finally creates
    data loaders to be served to the Pytorch Lightning Trainer.
    """
    def __init__(self, data_url: str, blueprint: SimpleNamespace) -> None:
        super().__init__()
        self.blueprint = blueprint
        self.data_url = data_url
        self.data_path = "data/" + hashlib.md5(data_url.encode("UTF-8")).hexdigest()
        self.train, self.val, self.test = None, None, None
        # default batch_size (will be overwritten by lightning trainer anyway)
        self.batch_size = 10

    def prepare_data(self):
        SapphireSet(url=self.data_url, root=self.data_path)

    def setup(self):
        transformations = list(map(lambda tf: all_transformations[tf], self.blueprint.transformations))
        transformations = transforms.Compose(*transformations)

        target_transformations = list(map(lambda tf: all_transformations[tf], self.blueprint.target_transformations))
        target_transformations = transforms.Compose(*target_transformations)

        self.train = SapphireSet(url=self.data_url, root=self.data_path, mode="train", transform=transformations,
                                 target_transform=target_transformations)

        self.val = SapphireSet(url=self.data_url, root=self.data_path, mode="val", transform=transformations,
                               target_transform=target_transformations)

        self.test = SapphireSet(url=self.data_url, root=self.data_path, mode="test", transform=transformations,
                                target_transform=target_transformations)

    def train_dataloader(self):
        return DataLoader(self.train, batch_size=self.batch_size, shuffle=True, num_workers=8)

    def val_dataloader(self):
        return DataLoader(self.val, batch_size=self.batch_size, shuffle=False, num_workers=8)

    def test_dataloader(self):
        return DataLoader(self.test, batch_size=self.batch_size, shuffle=False, num_workers=8)
