import torch
import pytorch_lightning as pl

from builder import build_blueprint


class SapphireModel(pl.LightningModule):
    def __init__(self, blueprint):
        super().__init__()
        self.blueprint = blueprint
        self.model, self.criterion, self.metrics = build_blueprint(blueprint)
        self.example_input_array = torch.rand(tuple(blueprint['borders']['input']))

    def forward(self, x):
        return self.model(x)

    def on_train_start(self):
        # self.logger.log_hyperparams({'lr': 1e-3, 'epochs': 5}, {'hp_metric/metric_1': 0, 'hp_metric/metric_2': 0})
        self.logger.log_graph(self, input_array=self.example_input_array)

    def training_step(self, batch, batch_idx):
        loss = self._step(batch)
        return {'loss': loss}

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        self.logger.experiment.add_scalars('epoch_loss', {'train': avg_loss}, global_step=self.global_step)

    def validation_step(self, batch, batch_idx):
        loss = self._step(batch)
        return {'val_loss': loss}

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([x['val_loss'] for x in outputs]).mean()
        self.logger.experiment.add_scalars('epoch_loss', {'val': avg_loss}, global_step=self.global_step)

    def test_step(self, batch, batch_idx):
        loss = self._step(batch)
        self.log('test_loss', loss)

    # Pytorch might discovers these optimizers anyway. Then we could delete this method. But I am unsure about that.
    def configure_optimizers(self):
        optimizers = []
        for architecture in self.model:
            try:
                optimizers.append(architecture.optim)
            except AttributeError:
                pass
        return optimizers

    # helper functions
    def _step(self, batch):
        datapoints, labels = batch
        y_out = self(datapoints)
        loss = self.criterion(y_out, labels)
        for metric in self.metrics:
            self.log(metric.name, metric.calculate(y_out, labels))
        return loss
