import hashlib
import json
from types import SimpleNamespace


def get_blueprints(case):
    """
    Takes a case and return blueprints for suitable deep learning architectures.
    For an example of such a case and blueprint, refer to the mocks directory.

    :param SimpleNamespace case: A SimpleNamespace, encoding a case to be solved.
    :return: A list of SimpleNamespaces, where each element describes one suitable deep learning architecture.
    :rtype: list of SimpleNamespace
    """
    fp = open('mocks/blueprint_mock.json')
    blueprint = json.load(fp)
    blueprint['signature'] = hashlib.md5(str(blueprint).encode("UTF-8")).hexdigest()
    blueprint['timestamp'] = case['timestamp']
    return [blueprint]


def _determine_architectures():
    pass


def _determine_criterion():
    """

    :return: A string, representing the suggested loss function
    :rtype: str
    """
    pass


def _determine_metrics():
    """

    :return: A list of strings, representing metrics which are adequate for the problem setting.
    :rtype: list of str
    """
    pass


def get_training_option(blueprint):
    """
    Takes a blueprint and initial restrictions. The restrictions are updated throughout this function by user
    interactions. This function returns once the user decides on a training option. It launches a container and returns
    a URL to this container. The container runs on the selected environment and the training service is later deployed
    on this container.

    :param SimpleNamespace blueprint: A SimpleNamespace, representing an deep learning architecture blueprint.
    :return: A string, representing a REST-Endpoint to a container.
    :rtype: str
    """
    pass
